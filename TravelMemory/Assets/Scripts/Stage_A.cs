﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // UIコンポーネントの使用

public class Stage_A: MonoBehaviour {
	
	Button Stage1;
	AudioSource focusSE;
   
	void Start () {
		// ボタンコンポーネントの取得
        Stage1 = GameObject.Find("/Canvas/Stage1").GetComponent<Button>();
			
		// 最初に選択状態にしたいボタンの設定
		Stage1.Select ();
		AudioSource[] audioSources = GetComponents<AudioSource>();
		focusSE = audioSources[1];
	}

	void Update() {
		if (Input.GetButtonDown ("Vertical") || Input.GetButtonDown ("Horizontal")) {
			focusSE.PlayOneShot (focusSE.clip);
		}
		if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1)
			|| Input.GetMouseButtonUp (2)) {
			Stage1.Select ();
		} 
	}
} 