﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObject_MoveBlock : MonoBehaviour {
	public Vector3 velocityA = new Vector3 (3.0f, 0.0f, 0.0f);
	public Vector3 velocityB = new Vector3 (-3.0f, 0.0f, 0.0f);
	public float switchingTime = 5.0f;
	public float vt = 0;

	bool turn = false;
	float changeTime = 0.0f;

	void FixedUpdate() {
		if (changeTime <= 0.0f) {
			// Time.fixedTime = FixedUpdateを開始した時間
			// velocityAで指定した速度で移動を開始
			changeTime = Time.fixedTime;
			GetComponent<Rigidbody2D> ().velocity = velocityA;
		}

		// changeTimeが経過すると、velocityBの速度に切り替わる
		if (Time.fixedTime + vt > changeTime + switchingTime) {
			GetComponent<Rigidbody2D> ().velocity = turn ? velocityA : velocityB;
			turn = turn ? false : true;
			changeTime = Time.fixedTime;
		}
	}
}