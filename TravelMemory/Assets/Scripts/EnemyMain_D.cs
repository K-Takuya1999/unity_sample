﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMain_D : EnemyMain {
	// 外部パラメータ
	public int aiIfRUNTOPLAYER = 80;
	public int aiIfESCAPE = 20;

	public int damageAttack_A = 4;

	AudioSource attackSE;

	public override void Start () {
		AudioSource audioSource = GetComponent<AudioSource>();
		attackSE = audioSource;
	}

	// == コード(AI思考処理)
	public override void FixedUpdateAI() {
		// AIステート
		switch (aiState) {
		// ランダムに行動を選択するステート
		case ENEMYAISTS.ACTIONSELECT :	// 思考の拠点
			//　アクションの選択(各ステートが時間オーバーすると,このステートに戻る)
			int n = SelectRandomAIState();
			if (n < aiIfRUNTOPLAYER) {
				SetAIState(ENEMYAISTS.RUNTOPLAYER,3.0f);
			} else {
				SetAIState(ENEMYAISTS.WAIT,1.0f + Random.Range(0.0f,1.0f));
			}
			enemyCtrl.ActionMove(0.0f);
			break;

			// 一定時間(止まって)待つ
		case ENEMYAISTS.WAIT :	// 休憩
			// キャラがプレイヤーの方を向く
			enemyCtrl.ActionLookup(player,0.1f);
			enemyCtrl.ActionMove(0.0f);
			break;

			// 走ってプレイヤーに近づく
		case ENEMYAISTS.RUNTOPLAYER:	// 近寄る
			// プレイヤーの一定範囲内に近づいた場合
			if (!enemyCtrl.ActionMoveToNear (player, 2.0f)) {
				Attack_A ();
			} else {
				enemyCtrl.ActionMoveToNear(player, 7.0f);
			}
			break;

			// プレイヤーから一定の距離以上に離れて逃げる
		case ENEMYAISTS.ESCAPE :	// 遠ざかる
			if(!enemyCtrl.ActionMoveToFar(player,5.0f)) {
				SetAIState(ENEMYAISTS.ACTIONSELECT,1.0f);
			}
			break;
		}
	}

	// 攻撃アクション
	void Attack_A() {
		enemyCtrl.ActionLookup(player, 0.1f);
		enemyCtrl.ActionMove (0.0f);
		enemyCtrl.ActionAttack ("Attack_A", damageAttack_A);
		SetAIState(ENEMYAISTS.WAIT, 1.0f);
	}

	public void EnemyD_AttackSE() {
		attackSE.PlayOneShot (attackSE.clip);
	}
}
