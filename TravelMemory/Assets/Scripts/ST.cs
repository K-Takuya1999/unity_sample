﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ST: MonoBehaviour {

	AudioSource decideSE;

	void Start() {
		AudioSource[] audioSource = GetComponents<AudioSource>();
		decideSE = audioSource[0];
	}

	public void StringArgFunction(string s){
		if (PauseScript.pauseNow) {
			PlayerController.score = 0;
			PauseScript.pauseNow = false;
			PlayerChange.playerChange = true;
		}
		decideSE.PlayOneShot (decideSE.clip);
		SceneManager.LoadScene (s);
		PlayerController.initParam = true;
		PlayerChange.playerChange = true;
		PlayerController.gameOverNow = false;
		EnemyController.clearNow = false;
		StageTrigger_CheckPoint.inputAny = false;
		PlayerBodyCollider.triggerInput = false;
	}
}