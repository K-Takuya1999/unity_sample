﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseScript : MonoBehaviour {
	Button retry;
	//　ポーズした時に表示するUI
	[SerializeField]private GameObject pauseUI;
	AudioSource pauseSE;
	AudioSource focusSE;
	public static bool pauseNow = false;

	void Start () {
		Time.timeScale = 1f;
		AudioSource[] audioSource = GetComponents<AudioSource>();
		pauseSE = audioSource[1];
		focusSE = audioSource[2];
	}

	// 変更
	void Update () {
		if (Input.GetButtonDown ("Fire3") && !PlayerController.gameOverNow && !EnemyController.clearNow) {
			//　ポーズUIのアクティブ、非アクティブを切り替え
			pauseUI.SetActive (!pauseUI.activeSelf);

			//　ポーズUIが表示されてる時は停止
			if (pauseUI.activeSelf) {
				Time.timeScale = 0f;
				pauseNow = true;
				pauseSE.PlayOneShot (pauseSE.clip);
				retry = GameObject.Find ("Stage_SubCamera/HUD_Pause/Canvas/retry").GetComponent<Button> ();
				retry.Select ();
				if (GameObject.Find ("HUD_GameOver").GetComponent<MeshRenderer> ().enabled ||
					GameObject.Find ("HUD_CLEAR").GetComponent<MeshRenderer> ().enabled) {
					Time.timeScale = 1f;
					GameObject.Find ("HUD_Pause").GetComponent<MeshRenderer> ().enabled = false;
					return;
				}
			//　ポーズUIが表示されてなければ通常通り進行
			}else {
				Time.timeScale = 1f;
				pauseNow = false;
				pauseSE.PlayOneShot (pauseSE.clip);
			}
		}

		if (pauseUI.activeSelf) {
			if (Input.GetButtonDown ("Vertical")) {
				focusSE.PlayOneShot (focusSE.clip);
			}
			if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1)
				|| Input.GetMouseButtonUp (2)) {
				retry.Select ();
			} 
		}
	}
}