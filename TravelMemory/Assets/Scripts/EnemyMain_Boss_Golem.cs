﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMain_Boss_Golem : EnemyMain {
	// 外部パラメータ
	public int aiIfATTACKONSIGHT = 50;
	public int aiIfESCAPE = 50;

	public int damageAttack_A = 2;
	public int damageAttack_B = 3;

	public int hitAttack_A = 2;	// 2回(制限)
	public int fireAttack_B = 2;	// 2回(制限)
	public float waitAttack_B = 1.0f;	// 3秒間攻撃しないようにする

	public ParticleSystem golemAtkUp;

	// == 内部パラメータ
	int hitCountAttack_A = 0;
	int fireCountAttack_B = 0;

	// === キャッシュ
	GameObject boss;
	GameObject bossHud;
	LineRenderer hudHpBarBoss;

	// === コード(AI思考処理)
	public override void Start () {
		base.Start();
		boss = GameObject.Find ("EnemyBoss_Golem");
		bossHud = GameObject.Find ("BossHud");
		hudHpBarBoss = GameObject.Find ("HUD_HPBar_Boss").GetComponent<LineRenderer> ();
	}

	public override void Update () {
		base.Update ();

		// ステータス表示
		if (enemyCtrl.hp > 0) {
			hudHpBarBoss.SetPosition (1, new Vector3 (15.0f * ((float)enemyCtrl.hp /
				(float)enemyCtrl.hpMax), 0.0f, 0.0f));
		} else {
			if (bossHud != null) {
				bossHud.SetActive (false);
				bossHud = null;
				boss.AddComponent<EnemyController> ();
				boss.SendMessage ("GameClear");
			}
		}
	}

	public override void FixedUpdateAI() {
		// AIステート
		switch (aiState) {
		case ENEMYAISTS.ACTIONSELECT:	// 思考の起点
			int n = SelectRandomAIState ();
			if (n < aiIfATTACKONSIGHT) {
				SetAIState (ENEMYAISTS.ATTACKONSIGHT, 100.0f);
			} else if (n < aiIfATTACKONSIGHT + aiIfESCAPE) {
				SetAIState(ENEMYAISTS.ESCAPE, Random.Range(2.0f,5.0f));
			} else {
				SetAIState (ENEMYAISTS.WAIT, 1.0f + Random.Range (0.0f, 1.0f));
			}
			enemyCtrl.ActionMove (0.0f);

			// 攻撃力アップは体力が減ってから
			if (enemyCtrl.hp < enemyCtrl.hpMax / 2.0f) {
				golemAtkUp.gameObject.SetActive (true);
				damageAttack_A = 4;
				damageAttack_B = 6;
			}
			break;

		case ENEMYAISTS.WAIT:	// 休憩
			break;

		case ENEMYAISTS.ATTACKONSIGHT:	// その場で攻撃
			Attack_A ();
			break;

		case ENEMYAISTS.ESCAPE:	// ホーミング攻撃
			Attack_B();
			break;
		}
	}

	// === コード(アクション処理)
	void Attack_A() {
		enemyCtrl.ActionAttack ("Attack_A", damageAttack_A);
		hitCountAttack_A ++;
		if (hitCountAttack_A >= hitAttack_A) {
			hitCountAttack_A = 0;
			SetAIState (ENEMYAISTS.WAIT, 1.0f);
		}
	}

	void Attack_B() {
		enemyCtrl.ActionAttack ("Attack_B", damageAttack_B);
		fireCountAttack_B ++;
		// 指定した回数投げたら、一定時間投げないようにする
		if (fireCountAttack_B >= fireAttack_B) {
			fireCountAttack_B = 0;
			SetAIState(ENEMYAISTS.FREEZ,waitAttack_B);
		}
	}
}
