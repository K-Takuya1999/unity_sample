﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CAMERAHOMING {  // カメラのホーミングタイプ
	LERP,                   // カメラとターゲット座標を線形補完する
	STOP,			        // カメラを止める
}

public class CameraFollow : MonoBehaviour {

	// 外部パラメータ(inspector表示)
	[System.Serializable]
	public class Param {
		public CAMERAHOMING homingType = CAMERAHOMING.LERP;
		public Vector2 margin = new Vector2(2.0f, 2.0f);
		public Vector2 homing = new Vector2(0.1f, 0.2f);
	}
	public Param param;

	// == キャッシュ
	GameObject player;
	Transform playerTrfm;
	PlayerController playerCtrl;

	// コード
	void Awake() {
	}

	// 全てのゲームオブジェクトのUpdateが呼び出された後に処理
	void LateUpdate() {
		player = PlayerController.GetGameObject();
		playerTrfm = player.transform;
		playerCtrl = player.GetComponent<PlayerController>();
		float targetX = playerTrfm.position.x;
		float targetY = playerTrfm.position.y;
		float pX = transform.position.x;
		float pY = transform.position.y;

		/*プレイヤーキャラの向いている方向から、param.marginを加算した離れた場所に
        ターゲットを設定(前方視界)*/
		targetX = playerTrfm.position.x + param.margin.x * playerCtrl.dir;
		targetY = playerTrfm.position.y + param.margin.y;

		// カメラ移動(ホーミング)
		switch (param.homingType) {
		case CAMERAHOMING.LERP:
			pX = Mathf.Lerp(transform.position.x, targetX, param.homing.x);
			pY = Mathf.Lerp(transform.position.y, targetY, param.homing.y);
			break;
		case CAMERAHOMING.STOP:
			if (GameObject.Find ("StageBoss_Road")) {
				pY = 29.50724f;
			}
			break;
		}
		transform.position = new Vector3(pX, pY, transform.position.z);
		GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, 30.0f, 100.0f);
	}

	// コード(その他)
	public void SetCamera(Param cameraPara) {
		param = cameraPara;
	}
}
