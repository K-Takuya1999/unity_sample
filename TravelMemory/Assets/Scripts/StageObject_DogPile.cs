﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObject_DogPile : MonoBehaviour {
	public GameObject[] enemyList;	// ドッグパイルに関連付けたい敵キャラのリスト	CheckEnemy(敵キャラの生存確認)
	public GameObject[] destroyObjectList;	// 戦闘後に破壊するオブジェクトのリスト

	void Start () {
		// CheckEnemy関数を1秒に1回呼び出してチェックする
		InvokeRepeating ("CheckEnemy", 0.0f, 1.0f);
	}

	void CheckEnemy() {
		// 登録されている敵リストからの敵の生存状態を確認
		bool flag = true;
		foreach (GameObject enemy in enemyList) {
			if (enemy != null) {
				flag = false;
			}
		}

		//すべての敵が倒されているか?
		if (flag) {
			// 登録されている破壊物リストのオブジェクトを削除
			foreach(GameObject destroyObject in destroyObjectList) {
				destroyObject.AddComponent<Effect_FadeObject> ();
				destroyObject.SendMessage ("FadeStart");
				Destroy(destroyObject, 1.0f);
			}
			CancelInvoke("CheckEnemy");
		}
	}
}
