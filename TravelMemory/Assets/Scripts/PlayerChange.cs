﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChange : MonoBehaviour {
	public GameObject[] characters  = new GameObject[2];
	private int index = 0;
	public static bool attackUpInput = false;
	public static bool playerChange = true;	//trueのとき剣士・falseのとき魔導士
	AudioSource changeSE;

	void Start() {
		AudioSource[] audioSource = GetComponents<AudioSource>();
		changeSE = audioSource[0];
	}
		
	void FixedUpdate() {
        ActionChange();
	}

	// 交代
	public void ActionChange(){
		if (Input.GetButtonDown("Fire2") && !attackUpInput &&
			!GameObject.Find ("HUD_GameOver").GetComponent<MeshRenderer> ().enabled &&
			!GameObject.Find ("HUD_CLEAR").GetComponent<MeshRenderer> ().enabled) {
            index++;
            // 上限を超えたら0に戻す
            if (index >= 2) {
                index = 0;
            }
			changeSE.PlayOneShot (changeSE.clip);
            Vector3 tmpPos = GameObject.FindGameObjectWithTag("Player").transform.position;
			if (GameObject.Find("Player")) {
				playerChange = false;
                GameObject.Find("/ChoosePlayers/Player/PlayerSprite").transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                GameObject.Find("/ChoosePlayers/Player/PlayerSprite").GetComponent<SpriteRenderer>().color = new Color(255.0f, 255.0f, 255.0f);
				GameObject.Find("/ChoosePlayers/Player/Effect_Hit").GetComponent<SpriteRenderer>().color = new Color(255.0f, 255.0f, 255.0f,0.0f);
			} else if (GameObject.Find("PlayerS")) {
				playerChange = true;
                GameObject.Find("/ChoosePlayers/PlayerS/PlayerSprite").transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                GameObject.Find("/ChoosePlayers/PlayerS/PlayerSprite").GetComponent<SpriteRenderer>().color = new Color(255.0f, 255.0f, 255.0f);
				GameObject.Find("/ChoosePlayers/PlayerS/Effect_Hit").GetComponent<SpriteRenderer>().color = new Color(255.0f, 255.0f, 255.0f,0.0f);
            }
            // 全て非アクティブにする
            foreach (GameObject gamObj in characters){
                gamObj.SetActive(false);
            }
            characters[index].SetActive(true);
            GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(tmpPos.x, tmpPos.y, tmpPos.z);
		}
	}
}