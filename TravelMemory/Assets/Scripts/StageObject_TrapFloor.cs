﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObject_TrapFloor : MonoBehaviour {

	void FixedUpdate () {
		if (GameObject.Find ("Player")) {
			GetComponent<Rigidbody2D> ().mass = 5.0f;
			GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezePositionX |
				RigidbodyConstraints2D.FreezeRotation;
		} else {
			GetComponent<Rigidbody2D> ().mass = 0.5f;
			GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezePositionY | 
				RigidbodyConstraints2D.FreezeRotation;
		}
		if (transform.position.y < 20.0f) {
			Destroy (this.gameObject);
		}
	}
}
