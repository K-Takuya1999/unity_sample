﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMainS : MonoBehaviour {

	// == キャッシュ(componentの取得・検索動作は高速でない)
	PlayerController playerCtrl;

	// == コード(シーン起動時に一回呼び出される、検索の処理無効になり、高速化できる)
	void Awake () {
		playerCtrl = GetComponent<PlayerController>();
		return;	// ジャンプ処理直後に攻撃が発生しないように処理を停止
	}

	void Update () {
		// == 操作可能であるか？(プレイヤーが入力してもいい状態であるか？)
		if (!playerCtrl.activeSts) {
			return;
		}

		// == 横移動
		float joyMv = Input.GetAxis ("Horizontal");
		playerCtrl.ActionMove (joyMv);

		// == ジャンプ
		if (Input.GetButtonDown ("Jump")) {
			playerCtrl.ActionJumpS ();
		}

		// 攻撃
		if (Input.GetButtonDown ("Fire1")) {
			playerCtrl.ActionAttack ();
		}
	}
}
