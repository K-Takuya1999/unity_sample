﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBodyCollider : MonoBehaviour {
	PlayerController playerCtrl;
	//GameObject playerMove;
	//GameObject playerSMove;
	//bool particleCheck = false;
	//public ParticleSystem playerPar;
	//public ParticleSystem playerSPar;
	public ParticleSystem playerAtkUp;
	public ParticleSystem playerSAtkUp;

	public static bool triggerInput = false;
	public static bool playerSUp = false;
	bool scoreUpEnabled = true;
	AudioSource HealSE;
	AudioSource attackUpSE;
	AudioSource scoreUpSE;

	void Awake() {
		playerCtrl = transform.parent.GetComponent<PlayerController>();
		AudioSource[] audioSource = GetComponents<AudioSource>();
		HealSE = audioSource[0];
		attackUpSE = audioSource[1];
		scoreUpSE = audioSource[2];
		//playerMove = GameObject.Find("Player");
		//playerSMove = GameObject.Find("PlayerS");
	}
		
	void OnTriggerEnter2D(Collider2D other) {
		// トリガーチェック
		// 敵キャラの攻撃判定を取得したら,ダメージ処理を行う
		if (other.tag == "EnemyArm") {
			EnemyController enemyCtrl = other.GetComponentInParent<EnemyController> ();
			// 同じ攻撃で2回以上判定が起こらないようにする
			if (enemyCtrl.attackEnabled) {
				enemyCtrl.attackEnabled = false;
				// 攻撃を受けたと同時に,プレイヤーの向きを敵キャラに向ける
				playerCtrl.dir = (playerCtrl.transform.position.x < enemyCtrl.transform.position.x) ? +1 : -1;
				if (PlayerChange.playerChange) {	
					playerCtrl.ActionDamage (enemyCtrl.attackDamage);
				} else {
					playerCtrl.ActionDamageS (enemyCtrl.attackDamage);
				}
				// オブジェクトをEnemyArmBulletタグで判別
			} 
		} else if (other.tag == "EnemyArmBullet") {
			FireBullet fireBullet = other.transform.GetComponent<FireBullet> ();
			// 同じ攻撃で2回以上判定が起こらないようにする
			if (fireBullet.attackEnabled) {
				fireBullet.attackEnabled = false;
				// 攻撃を受けたと同時に,プレイヤーの向きを敵キャラに向ける
				playerCtrl.dir = (playerCtrl.transform.position.x < fireBullet.transform.position.x) ? +1 : -1;
				if (PlayerChange.playerChange) {	
					playerCtrl.ActionDamage (fireBullet.attackDamage);
				} else {
					playerCtrl.ActionDamageS (fireBullet.attackDamage);
				}
				Destroy (other.gameObject);
			}
		} else if (other.tag == "CameraTrigger") {
			Camera.main.GetComponent<CameraFollow> ().SetCamera (other.GetComponent<StageTrigger_Camera> ().param);
		} else if (other.tag == "EventTrigger") {
			// 踏んだゲームオブジェクトにOnTriggerEnter2D_PlayerEventメッセージを送信する
			triggerInput = true;
			other.SendMessage ("OnTriggerEnter2D_PlayerEvent", gameObject);
		} else if (other.tag == "CLEARTrigger") {
			CLEARTrigger clear = other.GetComponent<CLEARTrigger>();
			clear.clearTrigger ();
		} else if (other.tag == "Item") {
			if (other.name == "Item_Heal") {
				HealSE.PlayOneShot (HealSE.clip);
				if (PlayerChange.playerChange) {
					playerCtrl.SetHP (playerCtrl.hp + playerCtrl.hpMax / 3, playerCtrl.hpMax);
				} else {
					playerCtrl.SetHPS (playerCtrl.hpS + playerCtrl.hpMaxS / 3, playerCtrl.hpMaxS);
				}
			}
			if (other.name == "Item_AttackUp") {
				playerCtrl.attackUp = true;
				PlayerChange.attackUpInput = true;
				attackUpSE.PlayOneShot (attackUpSE.clip);
				if (PlayerChange.playerChange) {
					playerAtkUp.gameObject.SetActive (true);
				} else {
					playerSAtkUp.gameObject.SetActive (true);
					playerSUp = true;
				}
				Invoke ("AttackUpEnd", 7.0f);
			}
			if (other.name == "Item_ScoreUp") {
				scoreUpSE.PlayOneShot (scoreUpSE.clip);
				if (scoreUpEnabled) {
					scoreUpEnabled = false;
					PlayerController.score += 500;
					Invoke ("ScoreUp", 0.0f);
				}
			}
			Destroy (other.gameObject);
		} else if (other.tag == "EnemyBody") {
			EnemyController enemyCtrl = other.GetComponentInParent<EnemyController> ();
			playerCtrl.dir = (playerCtrl.transform.position.x < enemyCtrl.transform.position.x) ? +1 : -1;
			if (PlayerChange.playerChange) {
				playerCtrl.ActionDamage (enemyCtrl.attackDamage);
			} else {
				playerCtrl.ActionDamageS (enemyCtrl.attackDamage);
			}
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		if (!PlayerController.gameOverNow) {
			if (other.tag == "DamageObject") {
				float damage = other.GetComponent<StageObject_Damage> ().damage * Time.fixedDeltaTime;
				if (PlayerChange.playerChange) {
					playerCtrl.ActionDamage (damage);
					if (playerCtrl.SetHP (playerCtrl.hp - damage, playerCtrl.hpMax)) {
						playerCtrl.Dead ();
					}
				} else {
					playerCtrl.ActionDamageS (damage);
					if (playerCtrl.SetHPS (playerCtrl.hpS - damage, playerCtrl.hpMaxS)) {
						playerCtrl.Dead ();
					}
				}
			}
		}
	}

	void AttackUpEnd() {
		playerCtrl.attackUp = false;
		PlayerChange.attackUpInput = false;
		playerSUp = false;
		playerAtkUp.gameObject.SetActive (false);
		playerSAtkUp.gameObject.SetActive (false);
	}

	void ScoreUp() {
		scoreUpEnabled = true;
	}

	/*void enabledParticle(){
		playerPar.GetComponent<Renderer> ().enabled = true;
		playerSPar.GetComponent<Renderer> ().enabled = true;
	}*/

	/*void disabledParticle(){
	playerPar.GetComponent<Renderer> ().enabled = false;
	playerSPar.GetComponent<Renderer> ().enabled = false;
	particleCheck = true;
	}*/

    // 何かにヒットしていたら常に呼び出される
    void OnCollisionStay2D(Collision2D col) {
		if (!playerCtrl.jumped &&
		    (col.gameObject.tag == "Road" || col.gameObject.tag == "MoveObject" ||
		    col.gameObject.tag == "Enemy")) {
			//playerMove.AddComponent<Effect_Move>();
			//playerMove.SendMessage("enableParticle");
			//playerSMove.AddComponent<Effect_Move>();
			//playerSMove.SendMessage("enableParticle");
			/*playerPar.gameObject.SetActive(true);
			playerSPar.gameObject.SetActive(true);*/
			/*playerPar.GetComponent<Renderer> ().enabled = true;
			playerSPar.GetComponent<Renderer> ().enabled = true;*/
			/*if (particleCheck) {
				Invoke ("enabledParticle", 2.0f);
			} else {
				enabledParticle();
			}
		} else {
			//playerMove.AddComponent<Effect_Move>();
			//playerMove.SendMessage("disableParticle");
			//playerSMove.SendMessage("disableParticle");
			/*playerPar.gameObject.SetActive(false);
			playerSPar.gameObject.SetActive(false);*/
			/*playerPar.GetComponent<Renderer> ().enabled = false;
			playerSPar.GetComponent<Renderer> ().enabled = false;
			particleCheck = true;*/
			//disabledParticle();
		}
    }
}
