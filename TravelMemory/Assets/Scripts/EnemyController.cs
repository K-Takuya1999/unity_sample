﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : BaseCharacterController {
	// == 外部パラメータ(inspector表示)
	public float initHpMax = 5.0f;
	public float initSpeed = 6.0f;
	public bool jumpActionEnabled = true;
	public Vector2 jumpPower = new Vector2(0.0f,1500.0f);
	public int addScore = 500;
	public static bool clearNow = false;

	// == 外部パラメータ(攻撃状態や攻撃力の設定)
	[System.NonSerialized] public bool cameraRendered = false;
	[System.NonSerialized] public bool attackEnabled = false;
	[System.NonSerialized] public int attackDamage = 1;

	// アニメーションのハッシュ名
	public readonly static int ANISTS_Idle = Animator.StringToHash("Base Layer.Enemy_Idle");
	public readonly static int ANISTS_Run = Animator.StringToHash("Base Layer.Enemy_Run");
	public readonly static int ANISTS_Jump = Animator.StringToHash("Base Layer.Enemy_Jump");
	public readonly static int ANITAG_ATTACK = Animator.StringToHash("Attack");
	public readonly static int ANISTS_DMG_A = Animator.StringToHash("Base Layer.Enemy_DMG_A");
	public readonly static int ANISTS_Dead = Animator.StringToHash("Base Layer.Enemy_Dead");

	// キャッシュ
	PlayerController playerCtrl;
	Animator playerAnim;

    // サポート関数
    public static GameObject GetGameObjectEnemy() {
        return GameObject.FindGameObjectWithTag("Enemy");
    }

    public static Transform GetTranformEnemy() {
        return GameObject.FindGameObjectWithTag("Enemy").transform;
    }

	// 初期化処理とゲームオブジェクトの参照に必要なキャッシュの登録
	protected override void Awake () {
		base.Awake();

		hpMax = initHpMax;
		hp = hpMax;
		speed = initSpeed;
	}

	protected override void Update () {
		base.Update();
		playerCtrl = PlayerController.GetController();
	}

	protected override void FixedUpdateCharacter() {
		// cameraRenderedがオフのときはカメラに表示されないものとして処理
		if (!cameraRendered) {
			return;
		}
		// ジャンプチェック
		if (jumped) {
			// 着地チェック(A: 接地瞬間判定 B: 接地と時間による判定
			if ((grounded && !groundedPrev) || (grounded && Time.fixedTime > jumpStartTime + 1.0f)) {
				jumped = false;
			}
		}

		// キャラの方向
		transform.localScale = new Vector3 (basScaleX * dir, transform.localScale.y, transform.localScale.x);
	}

	// 基本アクション
	public bool ActionJump() {
		// ジャンプアクションが有効で,地面に接地していれば
		if (jumpActionEnabled && grounded && !jumped) {	
			animator.SetTrigger ("Jump");
			// 物理的影響を受けるようにAddForceでジャンプ
			GetComponent<Rigidbody2D>().AddForce(jumpPower);
			jumped = true;
			jumpStartTime = Time.fixedTime;
		}
		return jumped;
	}

	public void ActionAttack(string atkname, int damage) {
		// 攻撃フラグを有効にして,AIから指定された攻撃アニメーションを再生
		attackEnabled = true;
		attackDamage = damage;
		animator.SetTrigger (atkname);
	}

	public void ActionDamage() {
		int damage = 1;
		// 死んでいないことを確認
		if (hp <= 0) {
			return;
		}
		if(superArmor) {
			// スーパーアーマーの色変えアニメーションを再生
			animator.SetTrigger("SuperArmor");
		}
		if (!superArmor) {
			animator.SetTrigger("DMG_A");
		}

		damage *= (playerCtrl.attackUp ? 3 : 1);
		if (PlayerBodyCollider.playerSUp) {
			damage = 3;
		}
		if (SetHP (hp - damage, hpMax)) {
			// 死亡アクションを呼び出し
			Dead ();
			// 敵キャラに設定されている加算スコアを,プレイヤーの体力に基づいて計算
			int addScoreV = ((int)((float)addScore * ((playerCtrl.hp + playerCtrl.hpS) / (playerCtrl.hpMax + playerCtrl.hpMaxS))));
			PlayerController.score += addScoreV;
		}
	}

	public void GameClear () {
		clearNow = true;
		GameObject.Find("HUD_CLEAR").GetComponent<MeshRenderer>().enabled = true;
		GameObject.Find("HUD_CLEARShadow").GetComponent<MeshRenderer>().enabled = true;
	}

	// コード(その他)
	public override void Dead() {
		base.Dead ();
		Destroy (gameObject, 1.0f);
	}
}
