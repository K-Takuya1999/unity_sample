﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObject_DropFloor : MonoBehaviour {
	public float destroyPos = -20.0f;

	void Update () {
		if (transform.position.y < destroyPos) {
			Destroy (this.gameObject);
		}
	}
}
