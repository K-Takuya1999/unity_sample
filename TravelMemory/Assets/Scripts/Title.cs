﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // UIコンポーネントの使用

public class Title: MonoBehaviour {

	Button StageA1;
	AudioSource focusSE;

	void Start () {
		// ボタンコンポーネントの取得
		StageA1 = GameObject.Find("/Canvas/Start").GetComponent<Button>();

		// 最初に選択状態にしたいボタンの設定
		StageA1.Select ();
		AudioSource[] audioSources = GetComponents<AudioSource>();
		focusSE = audioSources[1];
	}

	void Update() {
		if (Input.GetButtonDown ("Vertical")) {
			focusSE.PlayOneShot (focusSE.clip);
		}
		if (Input.GetMouseButtonUp (0) || Input.GetMouseButtonUp (1)
		    || Input.GetMouseButtonUp (2)) {
			StageA1.Select ();
		} 
	}
} 