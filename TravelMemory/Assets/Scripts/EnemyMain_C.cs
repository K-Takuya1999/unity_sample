﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMain_C : EnemyMain {
	// 外部パラメータ
	public int aiIfATTACKONSIGHT = 50;
	public int aiIfRUNTOPLAYER = 10;
	public int aiIfESCAPE = 10;
	
	public int damageAttack_A = 1;

	public int fireAttack_A = 5;	// 5回(制限)
	public float waitAttack_A = 5.0f;	// 5秒間攻撃しないようにする
	AudioSource attackSE;

	// == 内部パラメータ
	int fireCountAttack_A = 0;

	public override void Start () {
		AudioSource audioSource = GetComponent<AudioSource>();
		attackSE = audioSource;
	}
	
	// == コード(AI思考処理)
	public override void FixedUpdateAI() {
		// AIステート
		switch (aiState) {
			// ランダムに行動を選択するステート
			case ENEMYAISTS.ACTIONSELECT :	// 思考の拠点
				//　アクションの選択(各ステートが時間オーバーすると,このステートに戻る)
				int n = SelectRandomAIState();
				if (n < aiIfATTACKONSIGHT) {
					SetAIState(ENEMYAISTS.ATTACKONSIGHT,100.0f);
				}else if (n < aiIfATTACKONSIGHT + aiIfRUNTOPLAYER) {
					SetAIState(ENEMYAISTS.RUNTOPLAYER,3.0f);
				}else if (n < aiIfATTACKONSIGHT + aiIfRUNTOPLAYER + aiIfESCAPE) {
					SetAIState(ENEMYAISTS.ESCAPE, Random.Range(2.0f,5.0f));
				}
				else {
					SetAIState(ENEMYAISTS.WAIT,1.0f + Random.Range(0.0f,1.0f));
				}
				enemyCtrl.ActionMove(0.0f);
				break;
			
			// 一定時間(止まって)待つ
			case ENEMYAISTS.WAIT :	// 休憩
				// キャラがプレイヤーの方を向く
				enemyCtrl.ActionLookup(player,0.1f);
				enemyCtrl.ActionMove(0.0f);
				break;

			case ENEMYAISTS.ATTACKONSIGHT :	// その場で攻撃
				Attack_A();
				break;

			// 走ってプレイヤーに近づく
			case ENEMYAISTS.RUNTOPLAYER :	// 近寄る
				// プレイヤーの一定範囲内に近づいた場合
				if (!enemyCtrl.ActionMoveToNear(player,10.0f)) {
					Attack_A();
				}
				break;

			// プレイヤーから一定の距離以上に離れて逃げる
			case ENEMYAISTS.ESCAPE :	// 遠ざかる
				if(!enemyCtrl.ActionMoveToFar(player,4.0f)) {
					Attack_A();
				}
				break;
			}
	}
	
	// 攻撃アクション
	void Attack_A() {
		enemyCtrl.ActionLookup(player, 0.1f);
		enemyCtrl.ActionMove (0.0f);
		enemyCtrl.ActionAttack ("Attack_A", damageAttack_A);

		fireCountAttack_A ++;
		// 指定した回数投げたら、一定時間投げないようにする
		if (fireCountAttack_A >= fireAttack_A) {
			fireCountAttack_A = 0;
			SetAIState(ENEMYAISTS.FREEZ,waitAttack_A);
		}
	}

	public void EnemyC_AttackSE() {
		attackSE.PlayOneShot (attackSE.clip);
	}
}
