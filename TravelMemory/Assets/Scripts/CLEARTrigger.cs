﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CLEARTrigger : MonoBehaviour {
	
	public void clearTrigger() {
		GameObject.Find("HUD_CLEAR").GetComponent<MeshRenderer>().enabled = true;
		GameObject.Find("HUD_CLEARShadow").GetComponent<MeshRenderer>().enabled = true;
	}
}
