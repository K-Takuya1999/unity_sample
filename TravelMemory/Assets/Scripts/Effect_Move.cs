﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect_Move : MonoBehaviour {

	ParticleSystem effectMove;
	Renderer effectMoveCheck;

	public void enableParticle() {
		//ParticleSystem.MinMaxGradient enPart = new ParticleSystem.MinMaxGradient();
		//enPart.mode = ParticleSystemGradientMode.Color;
		//enPart.color = new Color (149.0f, 97.0f, 52.0f,255.0f);
		effectMove = this.gameObject.GetComponent<ParticleSystem>();
		effectMoveCheck = effectMove.GetComponent<ParticleSystemRenderer>();
		effectMoveCheck.enabled = true;
		//effectMove = GetComponent<ParticleSystem>();
		//effectMoveCheck = GetComponent<Renderer>().enabled = true;
	}

	public void disableParticle() {
		//ParticleSystem.MinMaxGradient enPart = new ParticleSystem.MinMaxGradient();
		//enPart.mode = ParticleSystemGradientMode.Color;
		//enPart.color = new Color (0.0f, 0.0f, 0.0f,0.0f);
		effectMove = this.gameObject.GetComponent<ParticleSystem>();
		effectMoveCheck = effectMove.GetComponent<ParticleSystemRenderer>();
		effectMoveCheck.enabled = false;
		//effectMove = GetComponent<ParticleSystem>();
		//effectMoveCheck = GetComponent<Renderer>().enabled = false;
	}

}
