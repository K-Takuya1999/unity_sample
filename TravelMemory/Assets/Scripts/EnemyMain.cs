﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 共通のAI情報とAI処理を実装

public enum ENEMYAISTS // 敵のAIステート
{
	ACTIONSELECT,	// アクション選択(思考)
	WAIT,			// 一定時間(止まって)待つ
	RUNTOPLAYER,	// 走ってプレイヤーに近づく
	JUMPTOPLAYER,	// ジャンプしてプレイヤーに近づく
	ESCAPE,			// プレイヤーから逃げる
	ATTACKONSIGHT,	// その場から移動せずに攻撃する(遠距離攻撃用)
	FREEZ,			// 行動停止(ただし移動処理は継続する)
}

public class EnemyMain : MonoBehaviour {
	// == 外部パラメータ(inspector表示)
	// (SelectRandomAiState)ステートを選択するためのランダム値を返す
	public bool cameraSwitch = true;	// カメラ外の休止している世界
	public int debug_SelectRandomAiState = -1;	

	// == 外部パラメータ
	// 敵キャラがカメラ内に表示されている状態を保存している
	[System.NonSerialized] public bool cameraEnabled = false;	
	[System.NonSerialized] public ENEMYAISTS aiState = ENEMYAISTS.ACTIONSELECT;
	[System.NonSerialized] public GameObject dogPile;

	// == キャッシュ
	protected EnemyController enemyCtrl;
	protected GameObject player;
	protected PlayerController playerCtrl;

	// == 内部パラメータ
	protected float aiActionTimeLength = 0.0f;
	protected float aiActionTimeStart = 0.0f;
	protected float distanceToPlayer = 0.0f;
	protected float distanceToPlayerPrev = 0.0f;

	// == コード
	public virtual void Awake() {
		enemyCtrl = GetComponent<EnemyController>();
	}

	public virtual void Start() {
		// シーン内の全てのドッグパイルオブジェクトを探索
		StageObject_DogPile[] dogPileList = 
			GameObject.FindObjectsOfType<StageObject_DogPile>();
		foreach (StageObject_DogPile findDogPile in dogPileList) {
			// 敵キャラである自分を登録しているものがないか探す
			foreach (GameObject go in findDogPile.enemyList) {
				// 見つけたら,自分のドッグパイルとしてdogPileに登録
				if (gameObject == go) {
					dogPile = findDogPile.gameObject;
					break;
				}
			}
		}
	}

	public virtual void Update() {
		player = PlayerController.GetGameObject();
		playerCtrl = player.GetComponent<PlayerController>();
		cameraEnabled = false;
	}

	// 思考が実際に動作する
	public virtual void FixedUpdate() {
		if (BeginEnemyCommonWork()) {
			//敵キャラごとのAI動作を仮想関数で呼び出し
			FixedUpdateAI();	
			EndEnemyCommonWork();
		}
	}


	public virtual void FixedUpdateAI() {
	}

	// 体力チェックとアクションチェック
	// アニメーションを確認し,確実にステートが実行されるようにする
	public bool BeginEnemyCommonWork() {
		// 生きているか
		if (enemyCtrl.hp <= 0) {
			return false;
		}

		// 空中は強制実行(空中設置敵・エリアル対応)
		if (enemyCtrl.grounded) {
			// カメラに入っているか?
			if (cameraSwitch && !cameraEnabled) {
				// カメラに映っていない
				enemyCtrl.ActionMove(0.0f);
				enemyCtrl.cameraRendered = false;
				enemyCtrl.animator.enabled = false;
				// ゲーム全体の処理速度を上げるために休止(Animatorコンポーネントの動作を停止)
				GetComponent<Rigidbody2D>().Sleep();
				return false;
			}
		}
		enemyCtrl.animator.enabled = true;
		enemyCtrl.cameraRendered = true;

		// 状態チェック
		if (!CheckAction()) {
			return false;
		}

		return true;
	}

	// 問題が発生して限界時間を超えたらAIの思考をリセット
	public void EndEnemyCommonWork() {
		// アクションのリミット時間をチェック
		float time = Time.fixedTime - aiActionTimeStart;
		if (time > aiActionTimeLength) {
			aiState = ENEMYAISTS.ACTIONSELECT;
		}
	}

	// 攻撃,くらい,死亡アニメーション実行中のとき,別のアクションを実行させない
	// 敵キャラが行動しないようにする
	public bool CheckAction() {
		// 状態チェック
		AnimatorStateInfo stateInfo = enemyCtrl.animator.GetCurrentAnimatorStateInfo(0);
		if (stateInfo.tagHash == EnemyController.ANITAG_ATTACK ||
			stateInfo.fullPathHash == EnemyController.ANISTS_DMG_A ||
			stateInfo.fullPathHash == EnemyController.ANISTS_Dead) {
			return false;
		}
		return true;
	}

	public int SelectRandomAIState() {
#if UNITY_EDITOR
		if(debug_SelectRandomAiState　>= 0) {
			return debug_SelectRandomAiState;
		}
#endif
		return Random.Range(0,100 + 1);
	}

	public void SetAIState(ENEMYAISTS sts,float t){
		aiState = sts;
		aiActionTimeStart = Time.fixedTime;
		aiActionTimeLength = t;
	}

	/*public virtual void SetCombatAIState(ENEMYAISTS sts) {
		aiState = sts;
		aiActionTimeStart = Time.fixedTime;
		enemyCtrl.ActionMove(0.0f);
	}*/

	// (GetDistanePlayer)敵キャラとプレイヤーとの距離を返す
	public float GetDistanePlayer() {
		distanceToPlayerPrev = distanceToPlayer;
		distanceToPlayer = Vector3.Distance (transform.position, playerCtrl.transform.position);
		return distanceToPlayer;
	}

	// (IsChangeDistanePlayer)プレイヤーとの距離が指定した大きさより変化してないかチェックする
	public bool IsChangeDistanePlayer(float l) {
		return(Mathf.Abs (distanceToPlayer - distanceToPlayerPrev) > 1);
	}

	// 敵キャラとプレイヤーとのX軸方向の距離を返す
	public float GetDistanePlayerX() {
		Vector3 posA = transform.position;
		Vector3 posB = playerCtrl.transform.position;
		posA.y = 0; posA.z = 0;
		posB.y = 0; posB.z = 0;
		return Vector3.Distance(posA,posB);
	}

	// 敵キャラとプレイヤーとのY軸方向の距離を返す
	public float GetDistanePlayerY() {
		Vector3 posA = transform.position;
		Vector3 posB = playerCtrl.transform.position;
		posA.x = 0; posA.z = 0;
		posB.x = 0; posB.z = 0;
		return Vector3.Distance (posA,posB);
	}
}
